/**
 * A function that takes a single argument.
 */
type AnyFunction = (...args: any) => any;

/**
 * Return the union of an original object, with a new object containing a
 * single key and value. Equivalent with the type
 * `TOriginal & { [P in TKey]: TValue }`, but TypeScript seems to lose some
 * type information when using union types.
 */
type And<TOriginal, TKey extends string, TValue> = {
  [P in keyof TOriginal | TKey]: P extends keyof TOriginal
    ? TOriginal[P]
    : TValue;
};

interface Can {
  /** Returns whether it's possible to run this event. */
  can(): boolean;
}

interface EventOptions<TState extends string, TFn extends AnyFunction> {
  /** Optional array of strings describing from which states this event can be run. */
  from?: TState[];
  /** Optional string describing the next state after running this event. */
  to?: TState;
  /** Optional function to be executed when this event is run. */
  fn?: TFn;
}

interface Builder<TMachine, TState extends string> {
  /**
   * Add an event to the finite state machine.
   * @param eventName Name of the event. It will be used as a key to access the event function.
   * @param eventOptions Event options.
   */
  event<
    TEventName extends string,
    TEventState extends string,
    TFn extends AnyFunction = () => void
  >(
    // The event names 'current' and 'prev' are reserved. Also prevent entering the same event name twice.
    eventName: TEventName extends "current" | "prev" | keyof TMachine
      ? never
      : TEventName,
    eventOptions: EventOptions<TEventState, TFn>,
  ): Builder<And<TMachine, TEventName, TFn>, TState | TEventState>;

  /**
   * Create the finite state machine.
   */
  build(): FiniteStateMachine<TState, TMachine>;
}

type FiniteStateMachine<TState, TMachine> = {
  /** Get the current state */
  readonly current: TState;
  /** Get the previous state */
  readonly prev: TState;
} & {
  readonly [P in keyof TMachine]: TMachine[P] & Can;
};

interface FsmFunction {
  /**
   * Create a finite state machine builder.
   * @param initialState The initial state of the finite state machine.
   */
  <TState extends string>(initialState: TState): Builder<{}, TState>;
}

export let fsm: FsmFunction = <TState extends string>(
  // The user will call this function with the initial state. Reuse this
  // argument for the current state.
  current: string,

  // Declare variables as arguments for better minification.
  machine?: { [p: string]: string | (AnyFunction & Can) } & {
    current: string;
    prev: string;
  },
  builder?: Builder<any, string>,
): Builder<{}, TState> => {
  // Create the machine prototype, initially only with `current` and `prev`
  // properties. It will be modified when running `event()`.
  machine = {
    current,
    prev: current,
  };

  // Create the builder to be returned.
  builder = {
    event<TFn extends AnyFunction>(
      eventName: string,
      eventOptions: EventOptions<string, TFn>,
      // Declare variable as an argument for better minification.
      fn?: AnyFunction & Partial<Can>,
    ) {
      fn = (...args: any) => {
        if (!fn!.can!()) {
          // Use template string for first variable but not second, for better
          // minification.
          throw `Can't ${eventName} from ` + current;
        } else {
          if (eventOptions.to) {
            machine!.prev = current;
            machine!.current = current = eventOptions.to;
          }

          // If `event.fn()` is defined, then return its result, else return
          // undefined.
          return eventOptions.fn && eventOptions.fn(...args);
        }
      };

      fn.can = (
        // Declare variable as an argument for better minification.
        from?: string[],
      ) => {
        from = eventOptions.from;
        return !from || from.includes(current);
      };

      // Install the event on the machine.
      machine![eventName] = fn as AnyFunction & Can;

      // Normally we'd return `this`, but `builder` is an alias. Return this
      // alias for better minification.
      return builder!;
    },

    build: (
      // Use a dummy argument for better minification.
      dummy?: never,
    ) => machine!,
  };

  return builder as Builder<{}, TState>;
};
