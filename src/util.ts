import * as fs from "fs";

export abstract class AppError extends Error {
  constructor(
    readonly statusCode: number,
    readonly code: string,
    message: string,
  ) {
    super(message);
  }
}

export class NotFoundError extends AppError {
  constructor(message: string) {
    super(404, "URI_NOT_FOUND", message);
  }
}

export function getRandom() {
  return "" + Math.floor(Math.random() * 1e8);
}

export function delay(timeout: number) {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

export function readFileAsync(filename: string) {
  const encoding = /\.png$/.test(filename) ? undefined : "utf-8";

  return new Promise((resolve, reject) => {
    fs.readFile(__dirname + "/" + filename, encoding, (err, data) =>
      err ? reject(err) : resolve(data),
    );
  });
}

export function getProcessArg(type: "string", name: string): string | undefined;
export function getProcessArg(
  type: "string",
  name: string,
  defaultValue: string,
): string;
export function getProcessArg(type: "number", name: string): number | undefined;
export function getProcessArg(
  type: "number",
  name: string,
  defaultValue: number,
): number;
export function getProcessArg(type: "boolean", name: string): boolean;
export function getProcessArg(
  type: "string" | "number" | "boolean",
  name: string,
  defaultValue?: any,
) {
  const index = process.argv.indexOf(name);
  if (type === "boolean") {
    return index !== -1;
  } else if (index === -1) {
    return defaultValue;
  }

  const value = process.argv[index + 1];
  if (!value || value.startsWith("-")) {
    return defaultValue;
  } else if (type === "number") {
    return +value;
  } else {
    return value;
  }
}
