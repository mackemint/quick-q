/* globals $, location, Audio, fetch, alert */

$.fn.visibility = function(show) {
  return this.prop("hidden", !show);
};

$.fn.show = function() {
  return this.prop("hidden", false);
};

$.fn.hide = function() {
  return this.prop("hidden", true);
};

$(() => {
  $("#lobby-url").text(
    location.host + location.pathname.replace(/\/present$/, ""),
  );

  if (/[?&]debug(&|$)/.test(location.search)) {
    $("#debug").show();
  } else {
    $("#debug").remove();
  }
});

function show(section) {
  $(".section").hide();
  $(`#${section}`).show();
}

let lobbyTimeout = -1;
let game;
let gameStats;
let question;
/** @type {HTMLAudioElement|undefined} */
let playingAudio;
let presenterGame = {
  type: "",
  version: 1,
  title: "",
  lobbyMusic: "",
  questions: [
    {
      question: "",
      timeLimit: 1,
      options: [{ text: "" }, { text: "", correct: true }],
      countdownMusic: "",
      countdownMusicAudio: playingAudio,
      image: "",
    },
  ],
};
async function start() {
  try {
    presenterGame = JSON.parse($("#serialized").val());
  } catch (e) {
    alert(e);
    return;
  }

  if (presenterGame.lobbyMusic) {
    playingAudio = new Audio(presenterGame.lobbyMusic);
    playingAudio.loop = true;
    playingAudio.play();
  }

  // Preload all music.
  for (const question of presenterGame.questions) {
    if (question.countdownMusic) {
      question.countdownMusicAudio = new Audio(question.countdownMusic);
      question.countdownMusicAudio.preload = "auto";
    }
  }

  $("#lobby-pin").text("Connecting...");
  $("#lobby-players").text("");
  show("lobby");

  game = await fetchJson("./api/games/start");
  $("#lobby-pin").text(game.gameId);

  lobbyTimeout = setInterval(async () => {
    const lobby = await fetchJson(
      `./api/games/${game.gameId}/lobby?password=${game.password}`,
    );

    $("#lobby-players")
      .text("")
      .append(...lobby.players.map(player => $("<li>").text(player.name)));
  }, 1000);
}

async function nextQuestion() {
  clearInterval(lobbyTimeout);
  if (playingAudio) {
    playingAudio.pause();
  }

  const presenterQuestion = presenterGame.questions.find(x => !x.answered);
  if (!presenterQuestion) {
    showResult();
    return;
  }

  presenterQuestion.answered = true;
  question = undefined;
  const numberOfOptions = presenterQuestion.options.length;

  $("#question-answers, #question-result-bars").hide();
  $("#question-text").text(presenterQuestion.question);
  $("#option-1, #option-1-result").visibility(1 <= numberOfOptions);
  $("#option-2, #option-2-result").visibility(2 <= numberOfOptions);
  $("#option-3-4-container, #option-3-result").visibility(3 <= numberOfOptions);
  $("#option-4, #option-4-result").visibility(4 <= numberOfOptions);

  presenterQuestion.options.forEach((option, i) => {
    $(`#option-${i + 1}-text`).text(option.text);
  });

  $("#question-image").visibility(presenterQuestion.image);
  if (presenterQuestion.image) {
    $("#question-image img").attr("src", presenterQuestion.image);
  }

  const params = [
    `password=${game.password}`,
    `numberOfOptions=${numberOfOptions}`,
    `correctOptions=${presenterQuestion.options
      .map((x, i) => ({ correct: x.correct, option: i + 1 }))
      .filter(x => x.correct)
      .map(x => x.option)
      .join(",")}`,
    `timeLimit=${presenterQuestion.timeLimit}`,
  ];

  let timer = presenterQuestion.timeLimit;
  $("#question-timer").text(timer);
  show("question");

  await delay(4000);

  $("#question-answers, #question-timer").show();
  const timerTimeout = setInterval(() => {
    timer--;
    $("#question-timer").text(timer);
    if (question || timer === 0) {
      clearInterval(timerTimeout);
    }
  }, 1000);

  if (presenterQuestion.countdownMusicAudio) {
    // eslint-disable-next-line require-atomic-updates
    playingAudio = presenterQuestion.countdownMusicAudio;
    playingAudio.play();
  }

  gameStats = await fetchJson(
    `./api/games/${game.gameId}/question?${params.join("&")}`,
  );
  clearInterval(timerTimeout);

  // If there's one second or less left of the audio, let it play until
  // completion.
  if (playingAudio && timer > 1) {
    playingAudio.pause();
  }

  const totalSelectedCount =
    gameStats.latestQuestionStats.reduce(
      (sum, optionStats) => sum + optionStats.selectedCount,
      0,
    ) || 1;

  for (const optionStats of gameStats.latestQuestionStats) {
    const optionIndex = optionStats.option - 1;
    const option = presenterQuestion.options[optionIndex];
    if (option) {
      option.selectedCount = optionStats.selectedCount;
      const fraction = Math.max(
        0.1,
        optionStats.selectedCount / totalSelectedCount,
      );

      const optionResult = $(`#option-${optionStats.option}-result`).css(
        "height",
        100 * fraction + "%",
      );
      optionResult
        .find(".option-result-text")
        .text((option.correct ? "✔ " : "") + optionStats.selectedCount);
    }
  }

  $("#question-timer").hide();
  $("#question-result-bars").show();

  const imageAfterAnswer =
    presenterQuestion.image || presenterQuestion.imageAfterAnswer;
  $("#question-image").visibility(imageAfterAnswer);
  if (imageAfterAnswer) {
    $("#question-image img").attr("src", imageAfterAnswer);
  }
}

function showRank() {
  if (!gameStats) {
    nextQuestion();
    return;
  }

  const playerResults = calculatePlayerResults();

  const formatter = Intl.NumberFormat();
  $("#rank-table")
    .text("")
    .append(
      ...playerResults.map(x =>
        $("<tr>").append(
          $("<td>").text(x.playerName),
          $("<td>").text(formatter.format(x.points)),
        ),
      ),
    );

  const hasNextQuestion = presenterGame.questions.some(x => !x.answered);

  if (!hasNextQuestion) {
    $("#rank-next-button").hide();
    $("#rank-result-title").show();

    fetchJson(`./api/games/${game.gameId}/complete?password=${game.password}`);
  }

  show("rank");
}

function calculatePlayerResults() {
  return gameStats.stats
    .map(playerStats => {
      const correctAnswersCount = playerStats.answers.filter(x => x.correct)
        .length;

      let streakBonus = 0;
      const points = playerStats.answers
        .map((answer, questionIndex) => {
          if (answer.correct) {
            streakBonus += 50;
            return Math.round(
              1000 -
                Math.log2(answer.answerSpeedInMilliseconds / 500) * 75 +
                streakBonus,
            );
          } else {
            streakBonus = 0;
            return 0;
          }
        })
        .reduce((result, value) => result + value, 0);

      return {
        playerName: playerStats.playerName,
        points: points,
        correctAnswersCount: correctAnswersCount,
      };
    })
    .sort((a, b) => b.points - a.points)
    .slice(0, 5);
}

function showResult() {
  $("#results")
    .text("")
    .append(
      ...calculatePlayerResults()
        .slice(0, 3)
        .map(x =>
          $("<div>", { class: "results-player" }).append(
            $("<div>", { class: "results-player-name" }).text(x.playerName),
            $("<div>", { class: "results-player-points" }).text(
              `${x.points} points`,
            ),
            $("<div>", { class: "results-player-correct-count" }).text(
              `(${x.correctAnswersCount} out of ${presenterGame.questions.length})`,
            ),
          ),
        ),
    );
  show("results");
}

async function fetchJson(url, opts) {
  const response = await fetch(url, opts);
  return response.json();
}

function delay(timeout) {
  return new Promise(resolve => setTimeout(resolve, timeout));
}
